# Programowanie Zespołowe – Hotel

## Cel projektu: 

Stworzenie aplikacji desktopowej umożliwiają obsługę hotelu. Aplikacja pozwala na zarządzanie pracownikami hotelu oraz klientami. Posiada moduły dedykowane dla managerów, osób sprzątających, techników wykonujących naprawy oraz recepcjonistów.

## Interesariusze projektu:

Aplikacja skierowana jest głównie dla właścicieli hoteli, którzy chcą posiadać narzędzie do zarządzania oraz monitorowania pracy wykonywanej przez swoich pracowników.

## Dane gromadzone w projekcie:

Aplikacja przechowuje dane osobowe pracowników takie jak: imię nazwisko, e-mail, numer telefonu, stanowisko, PESEL, datę zatrudnienia oraz pensję. Dane te wyświetlane są tylko pracownikom z rolą Manager. 
Hasła które nadawane są pracownikom przechowywane są w bazie danych w postaci zaszyfrowanej przez szyfr bCrypt

Dane Klientów wprowadzane są przez użytkowników z rola Manager oraz Recepcjonista i tylko osoby z tymi rolami mogą je wyświetlić. Dane klientów przechowywane w bazie to: Imię, Nazwisko, E-Mail, Numer telefonu.


## Diagram ERD

![erd](images/erd.png "Diagram ERD")

## Wykorzystane Technologie:

**Backend:** Java + Spring Boot 

**Frontend:** Electron, Vue.js, TypeScript 



## Konta użytkowników:

\- w przypadku bazy z przykładowymi danymi

```
E-MAIL: <mariusz.pudzian@spurvago.com> HASŁO: pass ROLA: Manager

E-MAIL: <malysz321@spurvago.com> HASŁO: pass ROLA: Technik

E-MAIL: <kzielinski@spurvago.com> HASŁO: pass ROLA: Pokojówka

E-MAIL: <jkowalska@spurvago.com> HASŁO: pass ROLA: Recepcjonista
```

\- w przypadku pustej bazy

```
E-MAIL: <admin@spurvago.com> HASŁO: pass ROLA: Manager
```

## Interfejs Serwisu

### Ekran logowania

![loginScreen](images/loginScreen.png "Interfejs Serwisu")

### Ekran widoku klientów

![clientView](images/clientView.png "Widok Klientów")

### Tworzenie nowego klienta

![clientCreate](images/clientCreate.png "Tworzenie Klientów")

### Edycja klienta

![clientEdit](images/clientEdit.png "Edycja Klientów")

### Wyszukiwanie klienta 

![clientSearch](images/clientSearch.png "Wyszukiwanie Klientów")

### Ekran widoku pracowników

![employeeView](images/employeeView.png "Widok Pracowników")

### Dodawanie nowego pracownika

![employeeCreate](images/employeeCreate.png "Tworzenie Pracowników")

### Edycja istniejącego pracownika

![employeeEdit](images/employeeEdit.png "Edycja Pracowników")

### Wyświetlanie danych pracownika

![employeeFetch](images/employeeFetch.png "Podgląd Pracowników")

### Ekran widoku pokoi

![roomView](images/roomView.png "Widok Pokoi")

### Wyświetlanie danych pokoju

![roomFetch](images/roomFetch.png "Podgląd Pokoi")

### Edycja pokoju

![roomEdit](images/roomEdit.png "Edycja Pokoi")

### Ekran modułu pokojówki

![maidView](images/maidView.png "Widok Pokojówki")

### Tworzenie zgłoszenia usługi sprzątania

![maidCreate](images/maidCreate.png "Tworzenie usługi sprzątania")

### Edycja zgłoszenia sprzątania

![maidEdit](images/maidEdit.png "Edycja Pokojówki")

### Uzupełnianie minibaru

![maidFinalization](images/maidFinalization.png "Uzupełnianie minibaru")

### Ekran napraw

![repairView](images/repairView.png "Widok Napraw")

### Widok zgłoszenia naprawy

![repairFetch](images/repairFetch.png "Podgląd Napraw")

### Ekran realizacji naprawy

![repairFinalization](images/repairFinalization.png "Finalizacja Napraw")

### Edycja zgłoszenia naprawy

![repairCreate](images/repairCreate.png "Edycja Napraw")

### Ekran magazynu

![productView](images/productView.png "Widok Magazynu")

### Widok przedmiotu z magazynu

![productFetch](images/productFetch.png "Podgląd przedmiotu z Magazynu")

### Edycja przedmiotu z magazynu

![productEdit](images/productEdit.png "Edycja przedmiotu z Magazynu")

### Ekran rezerwacji

![accommodationView](images/accommodationView.png "Widok Rezerwacji")

### Podglad rezerwacji

![accommodationFetch](images/accommodationFetch.png "Podgląd Rezerwacji")

### Edycja rezerwacji

![accommodationEdit](images/accommodationEdit.png "Edycja Rezerwacji")

## Opis ról użytkowników:

**Manager** – Nadrzędna rola, ma dostęp do wszystkich modułów. Tworzy i zarządza kontami pracowników.

**Recepcjonista** – Rola zarządzająca klientami oraz mająca dostęp do zgłoszeń naprawy, które później finalizowane są przez techników. Ma również dostęp do modułu zarządzania pokojami.

**Technik** – Rola odpowiedzialna za finalizowanie zgłoszeń naprawy. W raportach realizacji ma możliwość załączenia opisu raportu, w którym opisuje sposób naprawy lub potrzebne części. W raporcie może umieścić również numer pokoju w którym wystąpiła usterka.

**Pokojówka** – Osoby należące do tej grupy odpowiedzialne są za utrzymywanie czystości w pokojach hotelowych. Użytkownicy mają dostęp do modułu pokojówki, gdzie realizowane są zgłoszenia dotyczące sprzątania pokoi. Zgłoszenia tworzą się automatycznie w momencie utworzenia rezerwacji. Użytkownicy z rolą „pokojówka” widzą tylko zgłoszenia niesfinalizowane. Pokojówka zajmuje się również uzupełnianiem minibaru w pokojach. Po uzupełnieniu minibaru do zgłoszenia zostaje przypisana pokojówka, która dokonała finalizacji i nie jest możliwa jego edycja.

Rola oraz imię i nazwisko aktualnie zalogowanego użytkownika wyświetlane jest w lewym dolnych rogu aplikacji

![logout](images/logout.png "Logout")

## Opis funkcjonalności:

- **Wyszukiwanie:** 

Każdy moduł w aplikacji posiada zaimplementowaną opcje wyszukiwania. Użytkownik ma możliwość wyświetlenia tylko interesujących pozycji pasujących do wpisanych słów kluczowych.
Przykład użycia na module pracowników:
![search](images/search.png "Wyszukiwanie")


- **Uzupełnianie minibaru**

Użytkownicy z rolą Pokojówka (lub Manager) mają możliwość uzupełniania minibaru w pokojach hotelowych. Po każdym uzupełnieniu minibaru stan magazynowy produktów odpowiednio zmniejszany jest w module magazynu. Po dodaniu produktów do minibaru zgłoszenie zyskuje imie i nazwisko finalizującego zgłoszenie, a jego edycja zostaje zablokowana.

![refill](images/refill.png "Uzupełnienie minibaru")

![afterRefill](images/afterRefill.png "Po uzupełnieniu")


- **Tworzenie rezerwacji**

Tworzenie rezerwacji dostępne jest tylko dla użytkowników z rolą manager lub recepcjonista. W rezerwacji wybierany jest klient, pokój oraz czas trwania rezerwacji.

![createAccommodation](images/createAccommodation.png "Tworzenie rezerwacji")

- **Dodawanie produktu do magazynu**

W module magazyn użytkownicy z rolą pokojówka lub manager mają możliwość dodawania produktów do magazynu, które zostają umieszczone przez pokojówkę w minibarze. 

![addProduct](images/addProduct.png "Tworzenie produktu")
